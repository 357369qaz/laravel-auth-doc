<?php
namespace Faed\LaravelAuthDoc\controller;

use Faed\LaravelAuthDoc\models\Api;
use Faed\LaravelAuthDoc\models\Group;
use Faed\LaravelAuthDoc\models\Param;
use Faed\LaravelAuthDoc\models\Project;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ProjectController
{
    /**
     * @param Request $request
     * @return Project|Model
     */
    public function store(Request $request)
    {
         return Project::getProject(
             $request->input('app_name'),
             $request->input('name'),
             $request->input('path'),
             $request->input('versions'),
         );
    }


    public function save(Request $request,$id)
    {
        $this->delApiParams($id);
        array_map(function ($result) use ($id){
            $group = Group::updateOrCreate([
                'project_id'=>$id,
                'name'=>$result['group'],
            ]);

            $api = Api::create([
                'group_id'=>$group->id,
                'name'=>$result['name'],
                'tables'=>$result['tables']??[],
                'path'=>$result['path'],
                'method'=>$result['method'],
                'return'=>$result['return']??null,
            ]);

            array_map(function ($parameter) use ($api){
                Param::create([
                    'api_id'=>$api->id,
                    'type'=>$parameter['type'],
                    'name'=>$parameter['name'],
                    'is_must'=>$parameter['is_must'],
                    'desc'=>@$parameter['desc'],
                    'example'=>@$parameter['example'],
                    'parameter_type'=>@$parameter['parameter_type'],
                ]);
            },$result['parameter']??[]);

        },$request->input());

    }

    public function delApiParams($id)
    {
        $groupIds=Group::where('project_id',$id)->pluck('id');
        $apiIds = Api::whereIn('group_id',$groupIds)->pluck('id');
        Param::whereIn('api_id',$apiIds)->delete();
        Api::destroy($apiIds);
        Group::destroy($groupIds);
    }


    public function nav(Request $request)
    {
        $config = Project::get();
        return view('auth-doc::api.nav',compact('config'));
    }

    public function index(Request $request)
    {
        $tree = Group::with(['children'=>function($query){
            $query->select([
                'id',
                'group_id',
                'name as title',
            ]);
        }])->when($request->input('project_id'),function (Builder $builder,$value){
            $builder->where('project_id',$value);
        })->get([
            'id',
            'project_id',
            'name as title',
        ]);
        return view('auth-doc::api.index',compact('tree'));
    }

    public function api(Request $request)
    {
        $result = Api::whereKey($request->input('api_id'))->paginate(1000);
        return $this->jsonMsg($result);
    }

    public function param(Request $request)
    {
        $result = Param::whereApiId($request->input('api_id'))->paginate(1000);
        return $this->jsonMsg($result);
    }

    function jsonMsg($data, $stateCode = 200, array $header = [], int $option = 0)
    {
        return response()->json([
            'code' => 0,
            'msg' => '正在请求中...',
            'count' => $data->total(),
            'data' => $data->items()
        ], $stateCode, $header, $option);
    }
}