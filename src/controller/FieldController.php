<?php
namespace Faed\LaravelAuthDoc\controller;

use Faed\LaravelAuthDoc\models\Field;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class FieldController
{
    /**
     * @param $id
     * @return Collection
     */
    public function index($id)
    {
        return Field::where('project_id',$id)->get(['val','name','com','table']);
    }

    public function save($id,Request $request)
    {
        Field::clear($id);
        Log::info('-----',$request->input());
        foreach ($request->input() as $datum) {
            Field::create(array_merge($datum, [
                'project_id' => $id,
            ]));
        }
    }
}