<?php
namespace Faed\LaravelAuthDoc\commands;
use Faed\LaravelAuthDoc\models\Field;
use Illuminate\Console\Command;

class AuthDocField extends Command
{
    protected $signature = 'api:field';

    protected $description = '生成api字段表,api字段注释数据源';


    public function __construct()
    {
        parent::__construct();

    }

    public function handle()
    {
        $field = Field::saveDatabaseColumns();
        $this->table(['名称','值','链接','来源表名称'],$field);

    }
}