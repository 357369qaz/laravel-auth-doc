<?php
namespace Faed\LaravelAuthDoc\commands;

use Faed\LaravelAuthDoc\models\Api;
use Faed\LaravelAuthDoc\models\Project;
use Faed\LaravelAuthDoc\services\ApiService;
use Illuminate\Console\Command;
use Illuminate\Routing\Router;

class AuthDoc extends Command
{
    protected $signature = 'api:send';

    protected $description = '生成api文档,发送到指定地址';

    /**
     * The router instance.
     *
     * @var Router
     */
    protected $router;



    public function __construct(Router $router)
    {
        parent::__construct();
        $this->router = $router;
    }

    public function handle()
    {
        $apiService = new ApiService($this->router);
        $apiService->getApis(config('authdoc.route'))->analysis()->send();

        $table = [];
        foreach ($apiService->analysis as $analysis){
            $table[] = ['group'=>$analysis['group'],'name'=>$analysis['name'],'path'=>$analysis['path']];
        }
        $this->table(['分组','名称','路由'],$table);
        $this->line('处理完成共:'.count($apiService->analysis).'个接口');
    }

}