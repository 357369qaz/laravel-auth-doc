<?php

return [
    //项目名称
    'name'=>env('APP_NAME','中文名称'),

    //app名称
    'app_name'=>env('APP_NAME'),

    //接口版本
    'versions'=>1,

    //接口请求地址
    'path'=>env('APP_URL'),

    //api接受地址
    'send'=>'http://127.0.0.1:8000',


    //发送api的前缀
    'route'=>['api'],


    //指定读取的mysql链接,指定过滤表
    'mysql' => [
        'mysql'=>['apis'],
    ],




    //是否执行迁移
    'is_migration'=>true,

    //是否继续记录返回缓存
    'is_cache'=>true,


    /**
     * 自动为你生成方法注释　解放你的双手
     * 方法替换　匹配到方法名称的时候会将　:value 替换成你的分组名称　key 为方法名称 value 为你制定的模板
     */
    'fun' => [
//        'index'=>'列表 :value (详细)',
    ],
];
