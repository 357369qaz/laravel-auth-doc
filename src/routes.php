<?php

use Faed\LaravelAuthDoc\controller\FieldController;
use Faed\LaravelAuthDoc\controller\ProjectController;
use Illuminate\Support\Facades\Route;

Route::post('/auth-doc/project',[ProjectController::class,'store']);
Route::post('/auth-doc/save/{id}',[ProjectController::class,'save']);
Route::get('/auth-doc/field/{id}',[FieldController::class,'index']);
Route::post('/auth-doc/field/{id}',[FieldController::class,'save']);
/*
|--------------------------------------------------------------------------
|  视图
|--------------------------------------------------------------------------
*/
Route::get('/auth-doc',[ProjectController::class,'index'])->name('auth.doc.index');
Route::get('/auth-doc/info',[ProjectController::class,'api'])->name('auth.doc.api');
Route::get('/auth-doc/param',[ProjectController::class,'param'])->name('auth.doc.param');
Route::get('/auth-doc/doc',[ProjectController::class,'nav']);
