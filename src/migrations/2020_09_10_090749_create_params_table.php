<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class CreateParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('params', function (Blueprint $table) {
            $table->id();
            $table->integer('api_id')->comment('接口参数');
            $table->string('type',20)->comment('url参数 query参数 body参数');
            $table->string('name')->nullable()->comment('参数名称');
            $table->string('is_must')->nullable()->comment('是否必填');
            $table->string('desc')->nullable()->comment('描述');
            $table->string('example')->nullable()->comment('事例');
            $table->string('parameter_type')->nullable()->comment('参数类型');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `params` comment '参数表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('params');
    }
}
