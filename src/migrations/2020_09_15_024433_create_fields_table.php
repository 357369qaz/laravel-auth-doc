<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->id();
            $table->integer('project_id')->comment('项目id');
            $table->string('name')->comment('名称');
            $table->string('val')->comment('值');
            $table->string('com')->nullable()->comment('链接');
            $table->string('table')->nullable()->comment('来源表名称');
            $table->string('route')->nullable()->comment('路由');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `fields` comment 'api字段字典表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups');
    }
}
