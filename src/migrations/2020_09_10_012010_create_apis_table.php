<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
class CreateApisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apis', function (Blueprint $table) {
            $table->id();
            $table->integer('group_id')->comment('项目id');
            $table->string('name')->nullable()->comment('方法名称');
            $table->string('path')->nullable()->comment('路由');
            $table->string('method')->nullable()->comment('请求方式');
            $table->string('desc')->nullable()->comment('描述');
            $table->text('return')->nullable()->comment('成功返回');
            $table->string('tables')->nullable()->comment('字段表');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `apis` comment 'api接口表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apis');
    }
}
