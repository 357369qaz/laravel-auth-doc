<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('项目名称');
            $table->string('app_name')->nullable()->comment('app_name');
            $table->string('path')->nullable()->comment('请求地址');
            $table->string('versions')->default(1)->comment('版本');
            $table->timestamps();
        });
        DB::statement("ALTER TABLE `projects` comment '项目表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
