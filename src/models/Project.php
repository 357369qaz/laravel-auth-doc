<?php

namespace Faed\LaravelAuthDoc\models;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

/**
 * Faed\LaravelAuthDoc\models\Project
 *
 * @property int $id
 * @property string $name 项目名称
 * @property string|null $app_name app_name
 * @property string|null $path 请求地址
 * @property string $versions 版本
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Project newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Project newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Project query()
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereAppName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereVersions($value)
 * @mixin \Eloquent
 */
class Project extends Model
{
    protected $guarded = [];

    public static function getProject($app_name,$name,$path,$versions)
    {
        return Project::updateOrCreate([
            'app_name'=>$app_name,
            'versions'=>$versions,
        ],[
            'name'=>$name,
            'path'=>$path,
        ]);
    }

    public static function getProjectId()
    {
        $client = new Client();
        $project = $client->post(config('authdoc.send') . '/auth-doc/project', [
            'form_params' => [
                'app_name'=>config('authdoc.app_name'),
                'name'=>config('authdoc.name'),
                'path'=>config('authdoc.path'),
                'versions'=>config('authdoc.versions'),
            ]
        ]);
        $project = json_decode($project->getBody()->getContents(),true);
        return $project['id'];
    }
}
