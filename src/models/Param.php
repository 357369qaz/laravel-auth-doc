<?php

namespace Faed\LaravelAuthDoc\models;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

/**
 * Faed\LaravelAuthDoc\models\Param
 *
 * @property int $id
 * @property int $api_id 接口参数
 * @property int $type 1.url参数 2.query参数 3.body参数
 * @property string|null $name 参数名称
 * @property string|null $is_must 是否必填
 * @property string|null $desc 描述
 * @property string|null $example 事例
 * @property string $parameter_type 参数类型
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Param newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Param newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Param query()
 * @method static \Illuminate\Database\Eloquent\Builder|Param whereApiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Param whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Param whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Param whereExample($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Param whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Param whereIsMust($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Param whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Param whereParameterType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Param whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Param whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Param extends Model
{
    protected $guarded=[];

    public static function getProjectParam($projectId)
    {
        $client = new Client();
        $param = $client->get(config('authdoc.send') . '/auth-doc/field/'.$projectId);
        return json_decode($param->getBody()->getContents(),true);
    }
}
