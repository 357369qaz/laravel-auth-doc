<?php


namespace Faed\LaravelAuthDoc\models;
use Doctrine\DBAL\Schema\SchemaException;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Faed\LaravelAuthDoc\models\Field
 *
 * @property int $id
 * @property int $project_id 项目id
 * @property string $name 名称
 * @property string $val 值
 * @property string|null $route 路由
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Field newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Field newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Field query()
 * @method static \Illuminate\Database\Eloquent\Builder|Field whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Field whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Field whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Field whereProjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Field whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Field whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Field whereVal($value)
 * @mixin \Eloquent
 */
class Field extends Model
{
    protected $guarded=[];

    public static $data;

    public static function saveDatabaseColumns()
    {
        self::lang();
        self::getDatabaseColumns();
        $projectId = Project::getProjectId();
        $client = new Client();
        $client->post(config('authdoc.send') . '/auth-doc/field/' . $projectId, [
            'form_params' => self::$data
        ]);
        return self::$data;

    }
    public static function getDatabaseColumns()
    {
        foreach (config('authdoc.mysql',['mysql']) as $com=>$filterTables){
            $tables = DB::connection($com)->getDoctrineSchemaManager()->listTableNames();
            $tables = array_diff($tables,$filterTables);

            $prefix = DB::connection($com)->getConfig('prefix');
            foreach ($tables as $table){
                $columns = DB::getDoctrineSchemaManager()->listTableDetails($table);
                $column = Schema::connection($com)->getColumnListing(str_replace($prefix,'',$table));
                try {
                    foreach ($column as $item){
                        $comment = $columns->getColumn($item)->getComment();
                        if ($comment){
                            self::$data[] = [
                                'name'=>$item,
                                'val'=>$comment,
                                'com'=>$com,
                                'table'=>$table,
                            ];
                        }
                    }
                } catch (SchemaException $e) {
                    throw new \Exception('读取数据库字段错误:'.$e->getMessage());
                }
            }
        }

    }

    /**
     * 清理
     * @param $id
     */
    public static function clear($id)
    {
        Field::whereProjectId($id)->delete();
    }


    /**
     * 读取语言包
     */
    public static function lang()
    {
        $lang = include(resource_path('lang/'.config('app.locale').'/validation.php'));
        foreach ($lang['attributes']??[] as $name=>$val){
            self::$data[]=compact('name','val');
        }
    }
}