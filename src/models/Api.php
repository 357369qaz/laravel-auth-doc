<?php

namespace Faed\LaravelAuthDoc\models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Routing\Router;
use Illuminate\Support\Collection;

/**
 * Faed\LaravelAuthDoc\models\Api
 *
 * @property int $id
 * @property int $group_id 项目id
 * @property string|null $name 方法名称
 * @property string|null $path 路由
 * @property string|null $method 请求方式
 * @property string|null $desc 描述
 * @property string|null $return 成功返回
 * @property string|null $tables 表
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Faed\LaravelAuthDoc\models\Param[] $params
 * @property-read int|null $params_count
 * @method static \Illuminate\Database\Eloquent\Builder|Api newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Api newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Api query()
 * @method static \Illuminate\Database\Eloquent\Builder|Api whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Api whereDesc($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Api whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Api whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Api whereMethod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Api whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Api wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Api whereReturn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Api whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Api extends Model
{
    protected $guarded=[];
    protected $casts=[
        'tables'=>'array'
    ];
    public static $routes;

    public function params(): HasMany
    {
        return $this->hasMany(Param::class);
    }

    public function project()
    {
        return $this->hasOneThrough(Project::class,Group::class,'id','id','group_id','project_id');
    }

    public function getPathAttribute($value)
    {
        return optional($this->project)->path.'/'.$value;
    }

    public function getReturnAttribute($value)
    {
        if ($value){
            $projectId = optional($this->project)->id;
            $result =json_decode($value,true);
            $data = $this->getField($result,$projectId);
            return json_encode($data);
        }
        return json_encode(['暂无']);
    }

    public function getField($result,$projectId)
    {
        $data = [];
        foreach ($result as $key=>$value){
            if (is_array($value)){
                $data[$key] = $this->getField($value,$projectId);
            }else{
                $field = Field::where('project_id',$projectId)->where('name',$key)->whereIn('table',$this->tables)->value('val');
                if ($field){
                    $data[$key] = "$value(注释:{$field})";
                }else{
                    $data[$key] = $value;
                }
            }
        }
        return $data;
    }

}
