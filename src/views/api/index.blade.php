<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    {{--    <meta name="csrf-token" content="{{ csrf_token() }}">--}}
    <link rel="stylesheet" href="{{asset('/static/layuiadmin/layui/css/layui.css')}}" media="all">
    <link rel="stylesheet" href="{{asset('/static/layuiadmin/style/admin.css')}}" media="all">
</head>
<body>
<div class="layui-fluid">
    <input type="hidden" value="{{$tree}}" id="tree"/>

    <div class="layui-row layui-col-space15">
        <!-- 左树 -->
        <div class="layui-col-sm12 layui-col-md4 layui-col-lg3">
            <div class="layui-card">
                <div class="layui-card-body mini-bar" id="ltTree">

                </div>
            </div>
        </div>
        <!-- 右表 -->
        <div class="layui-col-sm12 layui-col-md8 layui-col-lg9">
            <div class="layui-card">
                <div class="layui-card-body">
                    <table class="layui-table" lay-skin="nob" id="apiTable"></table>
                </div>
            </div>

            <div class="layui-card">
                <div class="layui-card-body">
                    <table id="paramTable"></table>
                </div>
            </div>


            <div class="layui-card">
                <div class="layui-card-body mini-bar">
                    <pre class="intersays" id="result">

                    </pre>
                </div>
            </div>


        </div>
    </div>
</div>
<script src="{{asset('/static/layuiadmin/layui/layui.js')}}"></script>
<script>
    layui.use(['layer', 'form', 'tree', 'table'], function () {
        var $ = layui.jquery;
        var form = layui.form;
        var tree = layui.tree;
        var table = layui.table;
        var treeData = JSON.parse($('#tree').val());
        var apiTable = table.render({
            elem: '#apiTable'
            , url: "{{ route('auth.doc.api') }}"
            ,loading:true
            , cols: [[ //表头
                {field: 'name', title: '方法名称'}
                , {field: 'path', title: '请求地址'}
                , {field: 'method', title: '请求方式'}
                , {field: 'desc', title: '描述'}
            ]]
            ,done:function (res){
                var result =res.data[0].return;
                $('#result').html(result);
                $('.intersays').each(function (index, element){
                    $(this).html(syntaxHighlight(JSON.parse($(this).text())));
                });
            }
        });


        function syntaxHighlight(json) {
            if (typeof json != 'string') {
                json = JSON.stringify(json, undefined, 2);
            }
            json = json.replace(/&/g, '&').replace(/</g, '<').replace(/>/g, '>');
            return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function(match) {
                var cls = 'number';
                if (/^"/.test(match)) {
                    if (/:$/.test(match)) {
                        cls = 'key';
                    } else {
                        cls = 'string';
                    }
                } else if (/true|false/.test(match)) {
                    cls = 'boolean';
                } else if (/null/.test(match)) {
                    cls = 'null';
                }
                return '<span class="' + cls + '">' + match + '</span>';
            });
        }

        var paramTable = table.render({
            elem: '#paramTable'
            , url: "{{ route('auth.doc.param') }}"
            , cols: [[ //表头
                {field: 'name', title: '参数名称'}
                , {field: 'type', title: '参数类型'}
                , {field: 'parameter_type', title: '类型'}
                , {field: 'is_must', title: '是否必填'}
                , {field: 'desc', title: '描述'}
                , {field: 'example', title: '事例'}
            ]]
        });


        //常规用法
        tree.render({
            elem: '#ltTree' //默认是点击节点可进行收缩
            , accordion: true
            , data: treeData
            , click: function (obj) {
                console.log(obj);
                if (!obj.data.children) {
                    apiTable.reload({
                        where: {
                            api_id: obj.data.id,
                        }
                    });
                    paramTable.reload({
                        where: {
                            api_id: obj.data.id,
                        }
                    });
                }
            }
        });

        table.on('tool(dataTable)', function (obj) {
            if (obj.event === 'show') {
                paramTable.reload({
                    where: {
                        api_id: obj.data.id,
                    }
                });
            }
        });


    })

</script>

</body>
</html>
