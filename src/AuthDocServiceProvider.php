<?php
namespace Faed\LaravelAuthDoc;

use Faed\LaravelAuthDoc\commands\AuthDoc;
use Faed\LaravelAuthDoc\commands\AuthDocField;
use Illuminate\Support\ServiceProvider;

class AuthDocServiceProvider extends ServiceProvider
{


    public function boot()
    {
        //发布配置
        $this->publishes([
            $this->configPath() => config_path('authdoc.php'),
        ]);


        if (config('authdoc.is_migration')){
            //迁移文件
            $this->loadMigrationsFrom(__DIR__.'/migrations');
        }

        //发布命令
        if ($this->app->runningInConsole()) {
            $this->commands([
                AuthDoc::class,
                AuthDocField::class,
            ]);
        }

        //路由
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        $this->publishes([
            __DIR__.'/static' => public_path('static'),
        ], 'public');

        $this->loadViewsFrom(__DIR__.'/views','auth-doc');
    }


    public function register()
    {
        //发布配置
        $this->publishes([
            $this->configPath() => config_path('authdoc.php'),
        ]);
    }


    /**
     * Set the config path
     *
     * @return string
     */
    protected function configPath(): string
    {
        return __DIR__ . '/config/authdoc.php';
    }
}