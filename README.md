# [laravel-auth-doc](https://gitee.com/357369qaz/laravel-auth-doc.git)

> - #### 写文档非常浪费后端同学的时间
>
> - #### 自动读取 request 的验证规则和方法注释作为请求参数
>
> - #### 读取attributes,validation.php,数据库字段字段作为注释
>
> - #### 提供中间件保存返回会自动的生成相关文档

1. 安装

   ```shell
   composer require faed/laravel-auth-doc
   ```

2. 发布配置

   ```shell
   php artisan vendor:publish --provider="Faed\LaravelAuthDoc\AuthDocServiceProvider"
   ```

3. 配置

   ```php
   return [
       //项目名称
       'name'=>'laravel插件包开发',
   
       //app名称
       'app_name'=>env('APP_NAME'),
   
       //接口版本
       'versions'=>1,
   
       //接口请求地址
       'path'=>env('APP_URL'),
   
       //api接受地址
       'send'=>'http://127.0.0.1:8000',
   
       //发送api的前缀
       'route'=>['api'],
   
       //指定读取的mysql链接,指定过滤表
       'mysql' => [
           'mysql'=>['apis','fields'],
       ],
   
   
       //是否执行迁移
       'is_migration'=>true,
   
       //是否继续记录返回缓存
       'is_cache'=>true,
   
   
       /**
        * 自动为你生成方法注释　解放你的双手
        * 方法替换　匹配到方法名称的时候会将　:value 替换成你的分组名称　key 为方法名称 value 为你制定的模板
        */
       'fun' => [
   //        'index'=>'列表 :value (详细)',
       ],
   ];
   
   ```

4. 运行迁移文件生成相关的表

   ```shell
   php artisan migrate
   ```

5. 运行 自动生成相关的文档数据路由

   ```shell
   #生成api文档,发送到指定地址
   php artisan api:send
   #生成api字段表,api字段注释数据源
   php artisan api:field
   ```
# 文档编写

类注释自动分组,控制器

```php
/**
 * @group 用户管理 [group会做为该控制器分组名称,多个控制器可使用同一分组名称]    
 * @t groups [t会做为该控制器字段字典]
 * @package App\Http\Controllers
 */
```

```php
/**
 * 说明 [方法名称]
 * @t groups [t会做为该控制器字段字典,指定来源]
 * @q size n 大小 1 [query参数,格式->字段名称　是否必填　说明　事例]
 * @u page n 分页 1 [url参数,格式->字段名称　是否必填　说明　事例]
 * @b body y 参数 1 [body参数,格式->字段名称　是否必填　说明　事例]
 */
```

# 使用

1. http://xxxxx/auth-doc/doc 路由

   ![1656384183225](1656384183225.png)

2. 提供中间件[ RecordReturn ]记录返回数据,请自行添加,返回数据会自动匹配生成的相关字段注释

   ```php
      [
           'throttle:api',
           \Illuminate\Routing\Middleware\SubstituteBindings::class,
           RecordReturn::class,
      ];
   ```

   ![1656384475955](1656384475955.png)

> ps:可以作为一个项目多个版本的管理,或者单独布置多个项目接口管理

